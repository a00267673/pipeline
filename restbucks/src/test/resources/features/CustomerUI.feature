Feature: Customer UI

	Scenario: Successful login
		Given I am on the login page
		When I try to log in as user "danny" with password "password"
		Then I should be logged in as "danny"

	Scenario: Failed login
		Given I am on the login page
		When I try to log in as user "baduser" with password "badpass"
		Then I should get an unauthorized error

	Scenario: Order a drink
		Given I am on the login page
		When I try to log in as user "danny" with password "password"
		When I count how many orders there are before
		And I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I count how many orders there are after
		Then there is one more order
		And the latest order status is "PLACED"

	Scenario: Pay my bill
		Given I am on the login page
		When I try to log in as user "danny" with password "password"
		And I place an order for a "LARGE" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I pay for the latest order
		Then the latest order status is "PAID"
	
	Scenario: Cancel an order
		Given I am on the login page
		When I try to log in as user "danny" with password "password"
		And I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I count how many orders there are before
		And I delete the latest order
		And I count how many orders there are after
		Then there is one less order
