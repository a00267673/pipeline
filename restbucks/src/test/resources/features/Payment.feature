Feature: Pay my Bill
	Background:
		Given the database is empty
		And there is a customer called "danny" with password "mypass12"

	Scenario: Pay for my order
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		Then the order status is "PLACED"
		When I pay for my order
		Then I should get a "NO_CONTENT" response
		And the order status is "PAID"

	Scenario: Can't pay for an order twice
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I pay for my order
		And I pay for my order
		Then I should get a "CONFLICT" response
