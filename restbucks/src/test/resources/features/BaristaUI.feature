Feature: Barista UI

	Background:
		Given I log in as user "danny" with password "password"
		And I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I pay for the latest order

	Scenario: Update order status
		Given I log in as user "kamila" with password "password"
		And the latest order status is "PAID"
		When I try to update the order status to "SERVED"
		Then the latest order status is "SERVED"
