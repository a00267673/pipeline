Feature: Update an Order
	Background:
		Given the database is empty
		And there is a customer called "danny" with password "mypass12"

	Scenario: Change my order
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I change my order to "LARGE" "LATTE" "NO_MILK" "WITH_SUGAR"
		Then I should get a "OK" response
		And the order should be for a "LARGE" "LATTE" "NO_MILK" "WITH_SUGAR"

	Scenario: Can't change order after payment
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I pay for my order
		When I change my order to "LARGE" "LATTE" "NO_MILK" "WITH_SUGAR"
		Then I should get a "CONFLICT" response
		And the order should be for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"

	Scenario: Delete an order
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I delete my order
		Then I should get a "NO_CONTENT" response
		And the order should not exist
		
	Scenario: Can't delete order after after
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I pay for my order
		And I delete my order
		Then I should get a "CONFLICT" response
		And the order should be for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		