Feature: Barista can view/update orders

	Background:
		Given the database is empty
		And there is a "customer" called "danny" with password "mypass12"
		And there is a "customer" called "alice" with password "mypass12"
		And there is a "barista" called "kamila" with password "Test1234"
		And the following orders are created
		| id | customer | drinkSize | drinkType | withMilk  | withSugar   | status    |
		| 1  | danny    | SMALL     | AMERICANO | SOY_MILK  | NO_SUGAR    | PLACED    |
		| 2  | alice    | LARGE     | AMERICANO | NO_MILK   | WITH_SUGAR  | PLACED    |
		| 3  | alice    | LARGE     | LATTE     | WITH_MILK | NO_SUGAR    | PLACED    |
		| 4  | danny    | LARGE     | LATTE     | WITH_MILK | NO_SUGAR    | PAID      |
		| 5  | danny    | LARGE     | AMERICANO | SOY_MILK  | WITH_SUGAR  | SERVED    |
		| 6  | alice    | SMALL     | LATTE     | WITH_MILK | NO_SUGAR    | COLLECTED |

	Scenario: Barista can see all customer orders
		Given I log in as user "kamila" with password "Test1234"
		When I view customer orders
		Then I should see 6 orders

	Scenario: Can't view all orders if not logged in
		Given I am not logged in
		When I view customer orders
		Then I should get an unauthorized error

	Scenario: Can't view all orders if not logged in as a barista
		Given I log in as user "danny" with password "mypass12"
		When I view customer orders
		Then I should get an unauthorized error

	Scenario: View my account details
		Given I log in as user "kamila" with password "Test1234"
		When I try to view my account details
		Then I should see account details for user "kamila"

	Scenario: Can't view my account details if not logged in
		Given I am not logged in
		When I try to view my account details
		Then I should get an unauthorized error

	Scenario: Mark a PAID order as SERVED
		Given I log in as user "kamila" with password "Test1234"
		When I try to update status of order 4 as "SERVED"
		Then I should get a "NO_CONTENT" response
		And the status of order 4 is "SERVED"

	Scenario: Mark a SERVED order as COLLECTED
		Given I log in as user "kamila" with password "Test1234"
		When I try to update status of order 5 as "COLLECTED"
		Then I should get a "NO_CONTENT" response
		And the status of order 5 is "COLLECTED"

	Scenario: Can't mark a PLACED order as SERVED
		Given I log in as user "kamila" with password "Test1234"
		When I try to update status of order 1 as "SERVED"
		Then I should get a "CONFLICT" response
		And the status of order 1 is "PLACED"

	Scenario: Can't mark a COLLECTED order as SERVED
		Given I log in as user "kamila" with password "Test1234"
		When I try to update status of order 6 as "SERVED"
		Then I should get a "CONFLICT" response
		And the status of order 6 is "COLLECTED"
