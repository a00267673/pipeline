Feature: Drink Description unit test
	
	Scenario Outline:
		Given an order for a "<drinkSize>" "<drinkType>" "<withMilk>" "<withSugar>"
		Then the order should have description "<description>"
	Examples:
		| drinkSize | drinkType | withMilk  | withSugar   | description                         |
		| LARGE     | LATTE     | NO_MILK   | NO_SUGAR    | Large Latte                         |
		| SMALL     | AMERICANO | SOY_MILK  | NO_SUGAR    | Small Americano with soy milk       |
		| SMALL     | AMERICANO | NO_MILK   | WITH_SUGAR  | Small Americano with sugar          |
		| LARGE     | AMERICANO | WITH_MILK | WITH_SUGAR  | Large Americano with milk and sugar |
