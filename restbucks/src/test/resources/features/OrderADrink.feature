Feature: Order a Drink
	Background:
		Given the database is empty
		And there is a customer called "danny" with password "mypass12"

	Scenario: Order a drink
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		Then the order is created
		And I should get a receipt for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		
	Scenario: Attempt order with bad credentials
		Given I log in as user "baduser" with password "badpass"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		Then I should get an unauthorized error
		And the order is not created
		And I should not have a receipt

	Scenario: Attempt order without logging in
		Given I am not logged in
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		Then I should get an unauthorized error
		And the order is not created
		And I should not have a receipt

	Scenario: View my account details
		Given I log in as user "danny" with password "mypass12"
		When I try to view my account details
		Then I should see account details for user "danny"

	Scenario: Can't view my account details if not logged in
		Given I am not logged in
		When I try to view my account details
		Then I should get an unauthorized error

	Scenario: View my orders
		Given I log in as user "danny" with password "mypass12"
		When I place an order for a "SMALL" "AMERICANO" "WITH_MILK" "NO_SUGAR"
		And I place an order for a "LARGE" "LATTE" "NO_MILK" "WITH_SUGAR"
		And I view my orders
		Then I should see 2 orders

	Scenario: Can't view my orders if not logged in
		Given I am not logged in
		When I view my orders
		Then I should get an unauthorized error
