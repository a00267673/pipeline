package ie.ait.mase19.webdev.restbucks.test.restSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.rest.CustomerRS;
import ie.ait.mase19.webdev.restbucks.test.util.AuthHelper;

@Features("src/test/resources/features/OrderADrink.feature")
@RunWith(CukeSpace.class)
public class OrderADrinkTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(User.class, UserDAO.class, Order.class, OrderDAO.class, CustomerRS.class, AuthHelper.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private UserDAO users;

	@Inject
	private OrderDAO orders;

	@Inject
	private CustomerRS customerRS;

	private String authHeader;
	private Response response;

	@Given("^the database is empty$")
	public void the_database_is_empty() {
		users.deleteAll();
		orders.deleteAll();
		assertEquals(0, orders.count());
		assertEquals(0, users.count());
	}

	@Given("^there is a customer called \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void there_is_a_customer_called_with_password(final String username, final String password)
			throws Exception {
		User newUser = new User();
		newUser.setRole(User.Role.CUSTOMER);
		newUser.setUsername(username);
		newUser.setPassword(password);
		users.register(newUser);
	}

	@Given("^I log in as user \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void i_log_in_as_user_with_password(final String username, final String password) {
		authHeader = AuthHelper.makeBasicAuthHeader(username, password);
	}

	@Given("^I am not logged in$")
	public void i_am_not_logged_in() {
		authHeader = null;
	}

	@When("^I place an order for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_place_an_order_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		Order order = new Order();
		order.setDrinkSize(Order.DrinkSize.valueOf(drinkSize));
		order.setDrinkType(Order.DrinkType.valueOf(drinkType));
		order.setWithMilk(Order.WithMilk.valueOf(withMilk));
		order.setWithSugar(Order.WithSugar.valueOf(withSugar));
		response = customerRS.createOrder(authHeader, order);
	}

	@Then("^the order is created$")
	public void the_order_is_created() {
		assertEquals(1, orders.count());
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	}

	@Then("^the order is not created$")
	public void the_order_is_not_created() throws Throwable {
		assertEquals(0, orders.count());
		assertNotEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	}

	@Then("^I should get an unauthorized error$")
	public void i_should_get_an_unauthorized_error() {
		assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
	}
	
	@Then("^I should get a receipt for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_should_get_a_receipt_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		assertNotNull(response.getLocation());
		assertTrue(response.hasEntity());
		assertTrue(response.getEntity() instanceof Order);

		Order order = (Order) response.getEntity();
		assertNotNull(order.getId());
		assertEquals(drinkSize, order.getDrinkSize().toString());
		assertEquals(drinkType, order.getDrinkType().toString());
		assertEquals(withMilk, order.getWithMilk().toString());
		assertEquals(withSugar, order.getWithSugar().toString());
	}

	@Then("^I should not have a receipt$")
	public void i_should_not_have_a_receipt() throws Throwable {
		assertNull(response.getLocation());
		assertFalse(response.hasEntity());
	}

	
	@When("^I try to view my account details$")
	public void i_try_to_view_my_account_Details() throws Throwable {
		response = customerRS.lookupMyself(authHeader);
	}
	
	@Then("^I should see account details for user \\\"([^\\\"]*)\\\"$")
	public void i_should_see_account_details_for_user(final String username) {
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.hasEntity());
		assertTrue(response.getEntity() instanceof User);

		User user = (User) response.getEntity();
		assertNotNull(user.getId());
		assertEquals(username, user.getUsername());
	}
	
	@When("^I view my orders$")
	public void i_view_my_orders() throws Throwable {
		response = customerRS.listMyOrders(authHeader);
	}
	
	@Then("^I should see (\\d+) orders$")
	public void i_should_see_orders(int numberOfOrders) throws Throwable {
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.hasEntity());
		assertTrue(response.getEntity() instanceof List<?>);
		
		@SuppressWarnings("unchecked")
		List<Order> myOrders = (List<Order>) response.getEntity();
		assertEquals(numberOfOrders, myOrders.size());
	}
}
