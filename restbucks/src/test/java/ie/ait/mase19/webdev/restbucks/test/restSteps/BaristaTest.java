package ie.ait.mase19.webdev.restbucks.test.restSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.dto.UpdateStatusDTO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.rest.BaristaRS;
import ie.ait.mase19.webdev.restbucks.test.util.AuthHelper;

@Features("src/test/resources/features/Barista.feature")
@RunWith(CukeSpace.class)
public class BaristaTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(User.class, UserDAO.class, Order.class, OrderDAO.class, BaristaRS.class, AuthHelper.class, UpdateStatusDTO.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private UserDAO users;

	@Inject
	private OrderDAO orders;

	@Inject
	private BaristaRS baristaRS;

	private String authHeader;
	private Response response;
	// This maps IDs in our feature file to database IDs
	private Map<Long, Long> orderIdMap;

	@Given("^the database is empty$")
	public void the_database_is_empty() {
		users.deleteAll();
		orders.deleteAll();
		assertEquals(0, orders.count());
		assertEquals(0, users.count());
	}

	@Given("^there is a \\\"([^\\\"]*)\\\" called \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void there_is_a_customer_called_with_password(final String role, final String username, final String password)
			throws Exception {
		User newUser = new User();
		newUser.setRole(User.Role.valueOf(role.toUpperCase()));
		newUser.setUsername(username);
		newUser.setPassword(password);
		users.register(newUser);
	}
	
	@Given("^the following orders are created$")
	public void the_following_orders_are_created(DataTable ordersToCreate) {
		orderIdMap = new HashMap<Long, Long>();
		List<Map<String, String>> rows = ordersToCreate.asMaps(String.class, String.class);
		for (Map<String, String> columns : rows) {
			Long customerId = users.findByUsername(columns.get("customer")).getId();
			Order newOrder = new Order();
			newOrder.setCustomerId(customerId);
			newOrder.setDrinkSize(Order.DrinkSize.valueOf(columns.get("drinkSize")));
			newOrder.setDrinkType(Order.DrinkType.valueOf(columns.get("drinkType")));
			newOrder.setWithMilk(Order.WithMilk.valueOf(columns.get("withMilk")));
			newOrder.setWithSugar(Order.WithSugar.valueOf(columns.get("withSugar")));
			newOrder.setPrice(newOrder.calculatePrice());
			newOrder.setDateCreated(LocalDateTime.now().toString());
			newOrder.setStatus(Order.Status.valueOf(columns.get("status")));
			orders.register(newOrder);
			// Map the ID used in the feature file to the database one.
			Long orderId = Long.parseLong(columns.get("id"));
			Long dbOrderId = newOrder.getId();
			orderIdMap.put(orderId, dbOrderId);
		}
	}

	@Given("^I log in as user \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void i_log_in_as_user_with_password(final String username, final String password) {
		authHeader = AuthHelper.makeBasicAuthHeader(username, password);
	}

	@Given("^I am not logged in$")
	public void i_am_not_logged_in() {
		authHeader = null;
	}

	@Then("^I should get an unauthorized error$")
	public void i_should_get_an_unauthorized_error() {
		assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
	}
	
	@When("^I view customer orders$")
	public void i_view_my_orders() throws Throwable {
		response = baristaRS.listAllOrders(authHeader);
	}
	
	@Then("^I should see (\\d+) orders$")
	public void i_should_see_orders(int numberOfOrders) throws Throwable {
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.hasEntity());
		assertTrue(response.getEntity() instanceof List<?>);
		
		@SuppressWarnings("unchecked")
		List<Order> allOrders = (List<Order>) response.getEntity();
		assertEquals(numberOfOrders, allOrders.size());
	}
	
	@When("^I try to view my account details$")
	public void i_try_to_view_my_account_Details() throws Throwable {
		response = baristaRS.lookupMyself(authHeader);
	}
	
	@Then("^I should see account details for user \"([^\"]*)\"$")
	public void i_should_see_account_details_for_user(final String username) {
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertTrue(response.hasEntity());
		assertTrue(response.getEntity() instanceof User);

		User user = (User) response.getEntity();
		assertNotNull(user.getId());
		assertEquals(username, user.getUsername());
	}

	@When("^I try to update status of order (\\d+) as \"([^\"]*)\"$")
	public void i_try_to_update_status_of_order_as(final Long orderId, final String statusText) {
		Long dbOrderId = orderIdMap.get(orderId);
		UpdateStatusDTO updatedStatus = new UpdateStatusDTO();
		updatedStatus.setStatus(Order.Status.valueOf(statusText));
		response = baristaRS.updateOrderStatus(authHeader, dbOrderId, updatedStatus);
	}

	@Then("^I should get a \"([^\"]*)\" response")
	public void i_should_get_a_response(final String statusText) {
		Response.Status expectedResponse = Response.Status.valueOf(statusText);
		assertEquals(expectedResponse.getStatusCode(), response.getStatus());
	}

	@Then("^the status of order (\\d+) is \"([^\"]*)\"$")
	public void the_order_status_is(final Long orderId, final String statusText) {
		Long dbOrderId = orderIdMap.get(orderId);
		Response orderResponse = baristaRS.readOrder(authHeader, dbOrderId);
		Order order = (Order) orderResponse.getEntity();
		assertEquals(statusText, order.getStatus().toString());
	}
	
}
