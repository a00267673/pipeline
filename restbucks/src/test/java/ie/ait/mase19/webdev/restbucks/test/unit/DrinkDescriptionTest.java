package ie.ait.mase19.webdev.restbucks.test.unit;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.model.Order;

@Features("src/test/resources/features/DrinkDescription.feature")
@RunWith(CukeSpace.class)
public class DrinkDescriptionTest {
	
	private Order order;

	@Given("^an order for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void an_order_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		order = new Order();
		order.setDrinkSize(Order.DrinkSize.valueOf(drinkSize));
		order.setDrinkType(Order.DrinkType.valueOf(drinkType));
		order.setWithMilk(Order.WithMilk.valueOf(withMilk));
		order.setWithSugar(Order.WithSugar.valueOf(withSugar));
	}
	
	@Then("^the order should have description \"([^\"]*)\"$")
	public void the_order_should_have_description(final String description) {
		assertEquals(description, order.getDescription());
	}
	
}
