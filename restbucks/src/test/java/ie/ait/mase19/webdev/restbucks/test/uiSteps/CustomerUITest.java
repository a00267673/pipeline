package ie.ait.mase19.webdev.restbucks.test.uiSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.rest.CustomerRS;

@Features("src/test/resources/features/CustomerUI.feature")
@RunWith(CukeSpace.class)
public class CustomerUITest {

	@ArquillianResource
	URL contextPath;

	@Drone
	WebDriver driver;

	private static final long TIMEOUT = 5;

	private static final String WEBAPP_SRC = "src/main/webapp";

	private int rowsBefore, rowsAfter;

	@Deployment(testable = false)
	public static WebArchive createDeployment() {
		WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(CustomerRS.class, OrderDAO.class, UserDAO.class, Order.class, User.class)
				.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class).importDirectory(WEBAPP_SRC)
						.as(GenericArchive.class), "/", Filters.include(".*$"))
				.addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml")
//				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
//				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return war;
	}

	@Given("^I am on the login page$")
	public void i_am_on_the_login_page() {
		driver.get(contextPath + "signin.html");
	}

	@When("^I try to log in as user \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void i_try_to_log_in_as_user_with_password(final String username, final String password) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmLogin_Username")));
		driver.findElement(By.id("frmLogin_Username")).sendKeys(username);
		driver.findElement(By.id("frmLogin_Password")).sendKeys(password);
		driver.findElement(By.id("frmLogin_btnSignIn")).click();
	}

	@Then("^I should be logged in as \\\"([^\\\"]*)\\\"$")
	public void i_should_be_logged_in_as(final String username) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcomeText")));
		String welcomeText = driver.findElement(By.id("welcomeText")).getText();
		assertTrue(welcomeText.contains(username));
	}

	@When("^I place an order for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_place_an_order_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		driver.findElement(By.id("btnOrderCoffee")).click();
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmOrder_DrinkSize")));
		new Select(driver.findElement(By.id("frmOrder_DrinkSize"))).selectByValue(drinkSize);
		new Select(driver.findElement(By.id("frmOrder_DrinkType"))).selectByValue(drinkType);
		new Select(driver.findElement(By.id("frmOrder_WithMilk"))).selectByValue(withMilk);
		new Select(driver.findElement(By.id("frmOrder_WithSugar"))).selectByValue(withSugar);
		driver.findElement(By.id("frmOrder_btnPlaceOrder")).click();
	}

	@Then("^I should get an unauthorized error$")
	public void i_should_get_an_unauthorized_error() {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmLogin_Error")));
		assertTrue(driver.findElement(By.id("frmLogin_Error")).isDisplayed());
	}

	private int countOrders() {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders_info")));
		try {
			String info = driver.findElement(By.id("tblCustomerOrders_info")).getText();
			int startIndex = info.indexOf("of ") + 3;
			int endIndex = info.indexOf(' ', startIndex);
			return Integer.parseInt(info.substring(startIndex, endIndex));
		} catch (Exception e) {
			return 0;
		}
	}

	@When("^I count how many orders there are before$")
	public void i_count_how_many_orders_there_are_before() {
		rowsBefore = countOrders();
	}

	@When("^I count how many orders there are after$")
	public void i_count_how_many_orders_there_are_after() {
		rowsAfter = countOrders();
	}

	@Then("^there is one more order$")
	public void there_is_one_more_order() {
		assertEquals(rowsBefore + 1, rowsAfter);
	}

	@Then("^there is one less order$")
	public void there_is_one_less_order() {
		assertEquals(rowsBefore - 1, rowsAfter);
	}

	@Then("^the latest order status is \"([^\"]*)\"$")
	public void the_latest_order_status_is(String expectedStatus) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(5)")));
		String actualStatus = driver.findElement(By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(5)")).getText();
		assertEquals(expectedStatus, actualStatus);
	}

	@When("^I pay for the latest order$")
	public void i_pay_for_the_latest_order() {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders")));
		WebElement btnPay = driver.findElement(
				By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(6) > a.btnPayOrder"));
		btnPay.click();
		// Wait for modal
		wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmPayment_btnPay")));
		driver.findElement(By.id("frmPayment_btnPay")).click();
	}

	@When("^I delete the latest order$")
	public void i_delete_the_latest_order() {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders")));
		WebElement btnDelete = driver.findElement(
				By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(6) > a.btnDeleteOrder"));
		btnDelete.click();
		driver.switchTo().alert().accept();
	}
}