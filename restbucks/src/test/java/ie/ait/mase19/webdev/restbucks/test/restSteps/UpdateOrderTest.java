package ie.ait.mase19.webdev.restbucks.test.restSteps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.rest.CustomerRS;
import ie.ait.mase19.webdev.restbucks.test.util.AuthHelper;

@Features("src/test/resources/features/UpdateOrder.feature")
@RunWith(CukeSpace.class)
public class UpdateOrderTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(User.class, UserDAO.class, Order.class, OrderDAO.class, CustomerRS.class, AuthHelper.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private UserDAO users;

	@Inject
	private OrderDAO orders;

	@Inject
	private CustomerRS customerRS;

	private String authHeader;
	private Long orderId;
	private Response response;

	@Given("^the database is empty$")
	public void the_database_is_empty() {
		users.deleteAll();
		orders.deleteAll();
		assertEquals(0, orders.count());
		assertEquals(0, users.count());
	}

	@Given("^there is a customer called \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void there_is_a_customer_called_with_password(final String username, final String password)
			throws Exception {
		User newUser = new User();
		newUser.setRole(User.Role.CUSTOMER);
		newUser.setUsername(username);
		newUser.setPassword(password);
		users.register(newUser);
	}

	@Given("^I log in as user \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void i_log_in_as_user_with_password(final String username, final String password) {
		authHeader = AuthHelper.makeBasicAuthHeader(username, password);
	}

	@Given("^I am not logged in$")
	public void i_am_not_logged_in() {
		authHeader = null;
	}

	@When("^I place an order for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_place_an_order_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		Order order = new Order();
		order.setDrinkSize(Order.DrinkSize.valueOf(drinkSize));
		order.setDrinkType(Order.DrinkType.valueOf(drinkType));
		order.setWithMilk(Order.WithMilk.valueOf(withMilk));
		order.setWithSugar(Order.WithSugar.valueOf(withSugar));
		response = customerRS.createOrder(authHeader, order);
		orderId = ((Order) response.getEntity()).getId();
	}
	
	@When("^I change my order to \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_change_my_order_to(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		Order order = new Order();
		order.setDrinkSize(Order.DrinkSize.valueOf(drinkSize));
		order.setDrinkType(Order.DrinkType.valueOf(drinkType));
		order.setWithMilk(Order.WithMilk.valueOf(withMilk));
		order.setWithSugar(Order.WithSugar.valueOf(withSugar));
		response = customerRS.updateOrder(authHeader, orderId, order);
	}

	@When("^I pay for my order$")
	public void i_pay_for_my_order() {
		response = customerRS.makePayment(authHeader, orderId);
	}
	
	@Then("^the order status is \"([^\"]*)\"$")
	public void the_order_status_is(final String statusText) {
		Response orderResponse = customerRS.readOrder(authHeader, orderId);
		Order order = (Order) orderResponse.getEntity();
		assertEquals(statusText, order.getStatus().toString());
	}
	
	@Then("^the order should be for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void the_order_should_be_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		Response orderResponse = customerRS.readOrder(authHeader, orderId);
		Order order = (Order) orderResponse.getEntity();
		assertNotNull(order.getId());
		assertEquals(drinkSize, order.getDrinkSize().toString());
		assertEquals(drinkType, order.getDrinkType().toString());
		assertEquals(withMilk, order.getWithMilk().toString());
		assertEquals(withSugar, order.getWithSugar().toString());
	}
	
	@Then("^I should get a \"([^\"]*)\" response")
	public void i_should_get_a_response(final String statusText) {
		Response.Status expectedResponse = Response.Status.valueOf(statusText);
		assertEquals(expectedResponse.getStatusCode(), response.getStatus());
	}

	@When("^I delete my order$")
	public void i_delete_my_order() {
		response = customerRS.deleteOrder(authHeader, orderId);
	}

	@Then("^the order should not exist$")
	public void the_order_should_not_exist() {
		assertNull(orders.findById(orderId));
	}
}
