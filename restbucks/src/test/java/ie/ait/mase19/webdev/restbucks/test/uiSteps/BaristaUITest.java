package ie.ait.mase19.webdev.restbucks.test.uiSteps;

import static org.junit.Assert.assertEquals;
import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.dto.UpdateStatusDTO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.rest.BaristaRS;
import ie.ait.mase19.webdev.restbucks.rest.CustomerRS;

@Features("src/test/resources/features/BaristaUI.feature")
@RunWith(CukeSpace.class)
public class BaristaUITest {

	@ArquillianResource
	URL contextPath;

	@Drone
	WebDriver driver;

	private static final long TIMEOUT = 5;

	private static final String WEBAPP_SRC = "src/main/webapp";

	@Deployment(testable = false)
	public static WebArchive createDeployment() {
		WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
				.addClasses(CustomerRS.class, BaristaRS.class, UpdateStatusDTO.class, OrderDAO.class, UserDAO.class, Order.class, User.class)
				.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class).importDirectory(WEBAPP_SRC)
						.as(GenericArchive.class), "/", Filters.include(".*$"))
				.addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml")
//				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
//				.addAsWebInfResource("test-ds.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return war;
	}

	@When("^I log in as user \"([^\"]*)\" with password \"([^\"]*)\"$")
	public void i_log_in_as_user_with_password(final String username, final String password) {
		driver.get(contextPath + "signin.html");
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmLogin_Username")));
		driver.findElement(By.id("frmLogin_Username")).sendKeys(username);
		driver.findElement(By.id("frmLogin_Password")).sendKeys(password);
		driver.findElement(By.id("frmLogin_btnSignIn")).click();
	}

	@When("^I place an order for a \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_place_an_order_for_a(final String drinkSize, final String drinkType, final String withMilk,
			final String withSugar) {
		driver.findElement(By.id("btnOrderCoffee")).click();
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmOrder_DrinkSize")));
		new Select(driver.findElement(By.id("frmOrder_DrinkSize"))).selectByValue(drinkSize);
		new Select(driver.findElement(By.id("frmOrder_DrinkType"))).selectByValue(drinkType);
		new Select(driver.findElement(By.id("frmOrder_WithMilk"))).selectByValue(withMilk);
		new Select(driver.findElement(By.id("frmOrder_WithSugar"))).selectByValue(withSugar);
		driver.findElement(By.id("frmOrder_btnPlaceOrder")).click();
	}

	@Then("^the latest order status is \"([^\"]*)\"$")
	public void the_latest_order_status_is(String expectedStatus) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders")));
		String actualStatus = driver.findElement(By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(6)")).getText();
		assertEquals(expectedStatus, actualStatus);
	}

	@When("^I pay for the latest order$")
	public void i_pay_for_the_latest_order() {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders")));
		WebElement btnPay = driver.findElement(
				By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(6) > a.btnPayOrder"));
		btnPay.click();
		// Wait for modal
		wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmPayment_btnPay")));
		driver.findElement(By.id("frmPayment_btnPay")).click();
	}
	
	@When("^I try to update the order status to \"([^\"]*)\"$")
	public void i_try_to_update_the_order_status_to(final String newStatus) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tblCustomerOrders")));
		WebElement btnUpdate = driver.findElement(
				By.cssSelector("#tblCustomerOrders > tbody > tr:nth-child(1) > td:nth-child(7) > a"));
		btnUpdate.click();
		// Wait for modal
		wait = new WebDriverWait(driver, TIMEOUT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("frmUpdateStatus_Status")));
		new Select(driver.findElement(By.id("frmUpdateStatus_Status"))).selectByValue(newStatus);
		driver.findElement(By.id("frmUpdateStatus_btnUpdate")).click();
	}

}