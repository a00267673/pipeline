package ie.ait.mase19.webdev.restbucks.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;

/*
 * This API is used by customers for checking their own accounts. It uses the
 * authorization header to find user ID.
 */
@Path("/customer")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerRS {

	@Inject
	private UserDAO users;

	@Inject
	private OrderDAO orders;

	@GET
	public Response lookupMyself(@HeaderParam("authorization") String authString) {
		User authUser = users.findFromAuthHeader(authString);
		if (authUser == null)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		return Response.ok(authUser).build();
	}

	@GET
	@Path("/orders")
	public Response listMyOrders(@HeaderParam("authorization") String authString) {
		final User authUser = users.findFromAuthHeader(authString);
		if (authUser == null)
			return Response.status(Response.Status.UNAUTHORIZED).build();
		List<Order> myOrders = orders.findByCustomerId(authUser.getId());
		return Response.ok(myOrders).build();
	}

	@POST
	@Path("/orders")
	public Response createOrder(@HeaderParam("authorization") String authString, Order order) {
		User authUser = users.findFromAuthHeader(authString);
		if (authUser == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		} else if (order.getCustomerId() != null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		} else {
			order.setCustomerId(authUser.getId());
			order.setPrice(order.calculatePrice());
			order.setDateCreated(LocalDateTime.now().toString());
			order.setStatus(Order.Status.PLACED);
			orders.register(order);
			URI uri = null;
			try {
				uri = new URI("customer/orders/" + order.getId());
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			return Response.created(uri).entity(order).build();
		}
	}

	// Helper
	private Order getOrderIfAuthorized(final String authString, final long orderId) {
		final User authUser = users.findFromAuthHeader(authString);
		if (authUser != null) {
			final Order order = orders.findById(orderId);
			if (order != null && order.getCustomerId() == authUser.getId()) {
				return order;
			}
		}
		return null;
	}

	// Get order by ID
	@GET
	@Path("/orders/{orderId:[0-9][0-9]*}")
	public Response readOrder(@HeaderParam("authorization") String authString, @PathParam("orderId") long orderId) {
		final Order order = getOrderIfAuthorized(authString, orderId);
		if (order == null) return Response.status(Response.Status.UNAUTHORIZED).build(); 
		return Response.ok(order).build();
	}

	private List<String> collectErrorsForUpdateOrder(final Order originalOrder, final Order updatedOrder) {
		List<String> errors = new ArrayList<String>();
		if (updatedOrder.getId() != null) {
			errors.add("Order ID cannot be updated");
		} else {
			updatedOrder.setId(originalOrder.getId());
		}
		if (updatedOrder.getCustomerId() != null) { // || updatedOrder.getCustomerId() != originalOrder.getCustomerId()) {
			errors.add("Customer ID cannot be updated");
		} else {
			updatedOrder.setCustomerId(originalOrder.getCustomerId());
		}
		if (updatedOrder.getPrice() != null) {
			errors.add("Order price cannot be updated");
		} else {
			// XXX This will be recalculated following update.
			updatedOrder.setPrice(null);
		}
		if (updatedOrder.getDateCreated() != null) {
			errors.add("Order create date cannot be updated");
		} else {
			updatedOrder.setDateCreated(originalOrder.getDateCreated());
		}
		if (updatedOrder.getStatus() != null) {
			errors.add("Order status cannot be updated be customers");
		} else {
			updatedOrder.setStatus(originalOrder.getStatus());
		}
		return errors;
	}
	
	// Update order
	// TODO PUT should replace entire order, except ID.
	// No need for collecting errors.
	@PUT
	@Path("/orders/{orderId:[0-9][0-9]*}")
	public Response updateOrder(@HeaderParam("authorization") String authString, @PathParam("orderId") long orderId,
			Order updatedOrder) {
		final Order originalOrder = getOrderIfAuthorized(authString, orderId);
		if (originalOrder == null) return Response.status(Response.Status.UNAUTHORIZED).build();
		if (originalOrder.getStatus().equals(Order.Status.PLACED)) {
			List<String> errors = collectErrorsForUpdateOrder(originalOrder, updatedOrder);
			if (errors.isEmpty()) {
				// Recalculate price.
				updatedOrder.setPrice(updatedOrder.calculatePrice());
				orders.update(updatedOrder);
				return Response.ok(updatedOrder).build();
			} else {
				return Response.status(Response.Status.CONFLICT).entity(errors).build();
			}
		} else {
			String error = "Order can only be changed in PLACED state";
			return Response.status(Response.Status.CONFLICT).entity(error).build();
		}
	}

	// Delete order
	@DELETE
	@Path("/orders/{orderId:[0-9][0-9]*}")
	public Response deleteOrder(@HeaderParam("authorization") String authString, @PathParam("orderId") long orderId) {
		final Order order = getOrderIfAuthorized(authString, orderId);
		if (order == null) return Response.status(Response.Status.UNAUTHORIZED).build();
		if (order.getStatus().equals(Order.Status.PLACED)) {
			orders.delete(order);
			return Response.noContent().build();
		} else {
			String error = "Order can only be changed in PLACED state";
			return Response.status(Response.Status.CONFLICT).entity(error).build();
		}
	}
	
	// Pay for an order (dummy payment).
	@POST
	@Path("/orders/{orderId:[0-9][0-9]*}/payment")
	public Response makePayment(@HeaderParam("authorization") String authString, @PathParam("orderId") long orderId) {
		final Order order = getOrderIfAuthorized(authString, orderId);
		if (order == null) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		} else if (order.getStatus() != Order.Status.PLACED) {
			String error = "Order has already been paid";
			return Response.status(Response.Status.CONFLICT).entity(error).build();
		} else {
			order.setStatus(Order.Status.PAID);
			orders.update(order);
			return Response.noContent().build();
		}
	}

	// Calculate price
	@POST
	@Path("/price")
	public Response calculateOrderPrice(final Order order) {
		return Response.ok(order.calculatePrice()).build();
	}

}
