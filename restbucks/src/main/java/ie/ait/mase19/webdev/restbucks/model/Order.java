package ie.ait.mase19.webdev.restbucks.model;
// Sample comment
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Entity implementation class for Entity: Order
 *
 */
@Entity
@Table(name = "orders")
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Status {
		PLACED, PAID, SERVED, COLLECTED
	}

	public enum DrinkSize {
		SMALL, LARGE
	}

	public enum DrinkType {
		AMERICANO, LATTE
	}

	public enum WithMilk {
		NO_MILK, WITH_MILK, SOY_MILK
	}

	public enum WithSugar {
		NO_SUGAR, WITH_SUGAR
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "user_id")
	private Long customerId;

	@NotNull
	private String dateCreated;

	@NotNull
	private Double price;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Status status;

	@NotNull
	@Enumerated(EnumType.STRING)
	private DrinkSize drinkSize;

	@NotNull
	@Enumerated(EnumType.STRING)
	private DrinkType drinkType;

	@NotNull
	@Enumerated(EnumType.STRING)
	private WithMilk withMilk;

	@NotNull
	@Enumerated(EnumType.STRING)
	private WithSugar withSugar;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public DrinkSize getDrinkSize() {
		return drinkSize;
	}

	public void setDrinkSize(DrinkSize drinkSize) {
		this.drinkSize = drinkSize;
	}

	public DrinkType getDrinkType() {
		return drinkType;
	}

	public void setDrinkType(DrinkType drinkType) {
		this.drinkType = drinkType;
	}

	public WithMilk getWithMilk() {
		return withMilk;
	}

	public void setWithMilk(WithMilk withMilk) {
		this.withMilk = withMilk;
	}

	public WithSugar getWithSugar() {
		return withSugar;
	}

	public void setWithSugar(WithSugar withSugar) {
		this.withSugar = withSugar;
	}

	public String getDescription() {
		String drink = "";
		switch (drinkSize) {
		case LARGE:
			drink = "Large";
			break;
		case SMALL:
			drink = "Small";
			break;
		}
		switch (drinkType) {
		case AMERICANO:
			drink += " Americano";
			break;
		case LATTE:
			drink += " Latte";
			break;
		}
		String options = "";
		switch (withMilk) {
		case NO_MILK:
			break;
		case WITH_MILK:
			options = " with milk";
			break;
		case SOY_MILK:
			options = " with soy milk";
			break;
		}
		switch (withSugar) {
		case NO_SUGAR:
			break;
		case WITH_SUGAR:
			options += (options.isEmpty() ? " with sugar" : " and sugar");
			break;
		}
		return drink + options;
	}
	
	public double calculatePrice() {
		double price = 0.0;
		switch (drinkType) {
		case AMERICANO:
			price += 2.00;
			break;
		case LATTE:
			price += 2.80;
			break;
		}
		switch (drinkSize) {
		case LARGE:
			price += 1.0;
			break;
		case SMALL:
			break;
		}
		return price;
	}
}
