package ie.ait.mase19.webdev.restbucks.data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.util.List;

import ie.ait.mase19.webdev.restbucks.model.Order;

@Stateless
@LocalBean
public class OrderDAO {

	@PersistenceContext
	private EntityManager em;

	public void register(Order order) {
		em.persist(order);
	}

	public void update(Order order) {
		em.merge(order);
	}

	public void delete(Order order) {
		em.remove(em.contains(order) ? order : em.merge(order));
	}
	
	public void deleteAll() {
		em.createQuery("DELETE FROM Order").executeUpdate();
	}

	public Order findById(Long id) {
		return em.find(Order.class, id);
	}

	public List<Order> findAll() {
		Query query = em.createQuery("SELECT o FROM Order o");
		@SuppressWarnings("unchecked")
		List<Order> results = query.getResultList();
		return results;
	}

	public List<Order> findByCustomerId(Long userId) {
		Query query = em.createQuery("SELECT o FROM Order o WHERE o.customerId = :userId");
		query.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<Order> results = query.getResultList();
		return results;
	}

	public long count() {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(Order.class)));
		return em.createQuery(cq).getSingleResult();
	}
	
}
