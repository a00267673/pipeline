package ie.ait.mase19.webdev.restbucks.rest;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ie.ait.mase19.webdev.restbucks.data.OrderDAO;
import ie.ait.mase19.webdev.restbucks.data.UserDAO;
import ie.ait.mase19.webdev.restbucks.dto.UpdateStatusDTO;
import ie.ait.mase19.webdev.restbucks.model.Order;
import ie.ait.mase19.webdev.restbucks.model.User;
import ie.ait.mase19.webdev.restbucks.util.PATCH;

/*
 * This API is used by baristas for checking their own accounts. It uses the
 * authorization header to find user ID.
 */
@Path("/barista")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BaristaRS {

	@Inject
	private UserDAO users;

	@Inject
	private OrderDAO orders;

	private boolean hasBaristaAuth(final String authString) {
		final User authUser = users.findFromAuthHeader(authString);
		return (authUser != null && authUser.getRole() == User.Role.BARISTA);
	}

	@GET
	public Response lookupMyself(@HeaderParam("authorization") String authString) {
		final User authUser = users.findFromAuthHeader(authString);
		if (authUser == null || authUser.getRole() != User.Role.BARISTA) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		return Response.ok(authUser).build();
	}

	@GET
	@Path("/orders")
	public Response listAllOrders(@HeaderParam("authorization") String authString) {
		if (!hasBaristaAuth(authString)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		List<Order> allOrders = orders.findAll();
		return Response.ok(allOrders).build();
	}

	// Get order by ID
	@GET
	@Path("/orders/{orderId:[0-9][0-9]*}")
	public Response readOrder(@HeaderParam("authorization") String authString, @PathParam("orderId") long orderId) {
		if (!hasBaristaAuth(authString)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		final Order order = orders.findById(orderId);
		if (order == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok(order).build();
	}

	// Update Order Status
	@PATCH
	@Path("/orders/{orderId:[0-9][0-9]*}")
	public Response updateOrderStatus(@HeaderParam("authorization") String authString,
			@PathParam("orderId") long orderId, UpdateStatusDTO updatedStatus) {
		if (!hasBaristaAuth(authString)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
		Order originalOrder = orders.findById(orderId);
		if (originalOrder == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		
		if (originalOrder.getStatus().equals(updatedStatus.getStatus())) {
			return Response.notModified().build();
		}
		
		boolean updateValid = false;
		String errorMsg = null;
		switch (updatedStatus.getStatus()) {
		case PLACED:
			updateValid = false;
			errorMsg = "Order status can only be set to PLACED by creating a new order";
			break;
		case PAID:
			updateValid = false;
			errorMsg = "Order status can only be set to PAID by making a payment on a PLACED order";
			break;
		case SERVED:
			updateValid = (originalOrder.getStatus() == Order.Status.PAID);
			if (!updateValid) {
				errorMsg = "Order status can only be updated to SERVED from PAID";
			}
			break;
		case COLLECTED:
			updateValid = (originalOrder.getStatus() == Order.Status.SERVED);
			if (!updateValid) {
				errorMsg = "Order status can only be updated to COLLECTED from SERVED";
			}
			break;
		default:
			break;
		}
		if (updateValid) {
			originalOrder.setStatus(updatedStatus.getStatus());
			orders.update(originalOrder);
			return Response.noContent().build();
		} else {
			return Response.status(Response.Status.CONFLICT).entity(errorMsg).build();
		}
	}

}
