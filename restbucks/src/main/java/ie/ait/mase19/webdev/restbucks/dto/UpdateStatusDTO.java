package ie.ait.mase19.webdev.restbucks.dto;

import ie.ait.mase19.webdev.restbucks.model.Order;

public class UpdateStatusDTO {
	private Order.Status status;

	public Order.Status getStatus() {
		return status;
	}

	public void setStatus(Order.Status status) {
		this.status = status;
	}
}
