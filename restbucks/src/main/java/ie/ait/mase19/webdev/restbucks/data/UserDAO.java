package ie.ait.mase19.webdev.restbucks.data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.Base64;
import ie.ait.mase19.webdev.restbucks.model.User;

@Stateless
@LocalBean
public class UserDAO {

	@PersistenceContext
	private EntityManager em;

	public long count() {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(User.class)));
		return em.createQuery(cq).getSingleResult();
	}
	
	public void register(final User User) {
		em.persist(User);
	}
	
	public void deleteAll() {
		em.createQuery("DELETE FROM User").executeUpdate();
	}

	public User findById(final Long id) {
		return em.find(User.class, id);
	}

	public User findByUsername(final String username) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> criteria = cb.createQuery(User.class);
		Root<User> User = criteria.from(User.class);
		criteria.select(User).where(cb.equal(User.get("username"), username));
		try {
			return em.createQuery(criteria).getSingleResult();
		} catch (javax.persistence.NoResultException nre) {
			return null;
		}
	}


	public User findFromAuthHeader(final String authString) {
		if (authString != null && authString.toUpperCase().startsWith("BASIC ")) {
			String usernameAndPassword = new String(Base64.getDecoder().decode(authString.substring(6)));
			int colonIndex = usernameAndPassword.indexOf(':');
			if (colonIndex != -1) {
				String username = usernameAndPassword.substring(0, colonIndex);
				String password = usernameAndPassword.substring(colonIndex + 1);
				User user = findByUsername(username);
				if (user != null && user.getPassword().equals(password)) {
					return user;
				}
			}
		}
		return null;
	}

}
