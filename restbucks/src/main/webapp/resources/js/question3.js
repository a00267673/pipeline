document.addEventListener('DOMContentLoaded', function() {
	// Load authorization token set from the login page.
	var auth = window.sessionStorage.getItem('auth');

	// Populate the orders table.
	fetch('/restbucks/rest/barista/orders', {
		headers: { 'Authorization': 'Basic ' + auth }
	})
	.then(response => response.json())
	.then(orderList => {
		var html = '';
		orderList.forEach((order) => {
			html += '<tr>'
				+ '<td>' + order.id + '</td>'
				+ '<td>' + order.customerId + '</td>'
				+ '<td>' + order.description + '</td>'
				+ '<td>&euro;' + order.price.toFixed(2) + '</td>'
				+ '<td>' + order.dateCreated + '</td>'
				+ '<td>' + order.status + '</td>'
				+ '</tr>';
		});
		document.querySelector('#tblCustomerOrders > tbody').innerHTML = html;
	});
});
