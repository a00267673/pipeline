const baseUri = '/restbucks/rest';
var loggedInUser;

function btnSignOut_Click() {
	window.sessionStorage.setItem('auth', '');
	window.location = "signin.html";
}

function populateOrdersTable() {
	$.ajax({
		url : baseUri + '/barista/orders',
		method : 'GET',
		contentType : 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
		success : function(data) {
			$('#alertNoOrders').hide();
			$('#tblCustomerOrders').hide();
			var list = data;
			if (list.length == 0) {
				$('#alertNoOrders').show();
			} else {
				$('#tblCustomerOrders').DataTable().clear().destroy();
				$('#tblCustomerOrders tbody').html('');
				$.each(list, function(index, order) {
					var row = '<tr>';
					row += '<td>' + order.id + '</td>';
					row += '<td>' + order.customerId + '</td>';
					row += '<td>' + order.description + '</td>';
					row += '<td>&euro;' + order.price.toFixed(2) + '</td>';
					row += '<td>' + order.dateCreated + '</td>';
					var badgeType;
					switch (order.status) {
					case 'PLACED':
						badgeType = 'primary';
						break;
					case 'PAID':
						badgeType = 'success';
						break;
					case 'SERVED':
						badgeType = 'warning';
						break;
					case 'COLLECTED':
						badgeType = 'info';
						break;
					default:
						badgeType = 'info';
					}
					row += '<td><span class="badge badge-' + badgeType + '">' + order.status + '</span></td>';
					row += '<td>';
					if (order.status != 'COLLECTED') {
						row += '<a href="#" data-toggle="tooltip" title="Update status" class="btnUpdateStatus" onclick="tblCustomerOrders_btnUpdateStatus_Click(' + order.id + ',\'' + order.status + '\');"><i data-feather="edit"></i></a>';
					} else {
						row += '<a data-toggle="tooltip" title="Order cannot be updated after collection" class="disabled"><i data-feather="slash"></i></a>';
					}
					row += '</td>';
					row += '</tr>';
					$('#tblCustomerOrders tbody').append(row);
				});
				$('#tblCustomerOrders').DataTable({ order: [[ 4, 'desc' ]], searching: false, lengthChange: false });
				$('#tblCustomerOrders a').tooltip();  // load the tooltips
			    feather.replace();  // load the icons
				$('#tblCustomerOrders').show();
				$('#spnLastRefresh').text('Last refreshed ' + new Date().toLocaleString());
			}
		},
		error : function(response) {
			var message = response.status + " " + response.statusText;
			alert(message);
		},
		complete : function() {

		}
	});
}

function tblCustomerOrders_btnUpdateStatus_Click(orderId, status) {
	$('#frmUpdateStatus_Error').html();
	$('#frmUpdateStatus_Error').hide();
	$('#frmUpdateStatus_OrderID').val(orderId);
	$('#frmUpdateStatus_OldStatus').val(status);
	$('#frmUpdateStatus_Status').val(status);
	$('#updateStatusModal').modal();
}

function frmUpdateStatus_btnUpdate_Click() {
	var orderId = $('#frmUpdateStatus_OrderID').val();
	var order_json = JSON.stringify({
		status: $('#frmUpdateStatus_Status').val()
	});
    $.ajax({
        url: baseUri + '/barista/orders/' + orderId,
        method: 'PATCH',
        contentType: 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
        data: order_json,
        success: function(data, textStatus, jqXHR) {
        	if (jqXHR.status != 304) /* not modified */ {
        		populateOrdersTable();
        	}
            $('#updateStatusModal').modal('hide');
            $('#frmUpdateStatus_Error').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
			var message = '<b>' + jqXHR.status + ' ' + jqXHR.statusText + ':</b>'
				+ '<br>' + jqXHR.responseText;
        	$('#frmUpdateStatus_Error').html(message);
        	$('#frmUpdateStatus_Error').show();
        },
		complete : function() {
			
		}
    });
}

$(document).ready(function() {
	$('#btnRefreshOrders').click(populateOrdersTable);

	$('#frmUpdateStatus').submit(function(event){
		event.preventDefault();
		frmUpdateStatus_btnUpdate_Click();
	});
	
	$.ajax({
		url : baseUri + '/barista',
		method : 'GET',
		contentType : 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
		success : function(data) {
			loggedInUser = data;
			$('#welcomeText').html('Welcome, ' + loggedInUser.username + '!');
			populateOrdersTable();
		},
		error : function(response) {
			window.location = "signin.html";
		},
		complete : function() {

		}
	});
});