const baseUri = '/restbucks/rest';
var loggedInUser;

function btnSignOut_Click() {
	window.sessionStorage.setItem('auth', '');
	window.location = "signin.html";
}

function frmOrder_btnPlaceOrder_Click() {
	var order_json = JSON.stringify({
		drinkSize: $('#frmOrder_DrinkSize').val(),
		drinkType: $('#frmOrder_DrinkType').val(),
		withSugar: $('#frmOrder_WithSugar').val(),
		withMilk: $('#frmOrder_WithMilk').val()
	});
	$.ajax({
		url: baseUri + '/customer/orders',
		method: 'POST',
		contentType: 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
	    data: order_json,
	    success: function(response) {
	        console.log(response);
	        populateOrdersTable();
	    },
	    error: function(response) {
	    	var message = response.status + " " + response.statusText;
			alert(message);
		},
		complete : function() {
			$('#orderModal').modal('hide');
		}
	});
}

function frmOrder_btnUpdateOrder_Click() {
	var orderId = $('#frmOrder_UpdateOrderID').val();
	var order_json = JSON.stringify({
//		id: orderId,
//		customerId: loggedInUser.id,
		drinkSize: $('#frmOrder_DrinkSize').val(),
		drinkType: $('#frmOrder_DrinkType').val(),
		withSugar: $('#frmOrder_WithSugar').val(),
		withMilk: $('#frmOrder_WithMilk').val()
	});
    $.ajax({
        url: baseUri + '/customer/orders/' + orderId,
        method: 'PUT',
        contentType: 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
        data: order_json,
        success: function(response) {
            console.log(response);
            populateOrdersTable();
        },
        error: function(response) {
			var message = response.status + " " + response.statusText;
			alert(message);
        },
		complete : function() {
			$('#orderModal').modal('hide');
		}
    });
}

function frmOrder_Change() {
	var order_json = JSON.stringify({
		//customerId: loggedInUser.id,
		//drinkSize: $('input[name=drinkSize]:checked', '#frmOrder').val(), 
		drinkSize: $('#frmOrder_DrinkSize').val(),
		drinkType: $('#frmOrder_DrinkType').val(),
		withSugar: $('#frmOrder_WithSugar').val(),
		withMilk: $('#frmOrder_WithMilk').val()
	});
    $.ajax({
        url: baseUri + '/customer/price',
        method: 'POST',
        contentType: 'application/json',
        data: order_json,
        success: function(response) {
            $('#frmOrder_Price').val(response.toFixed(2));
        },
        error: function(response) {
			var message = response.status + " " + response.statusText;
			alert(message);
        }
    });
}

function populateOrdersTable() {
	$.ajax({
		url : baseUri + '/customer/orders',
		method : 'GET',
		contentType : 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
		success : function(data) {
			$('#alertNoOrders').hide();
			$('#tblCustomerOrders').hide();
			var list = data;
			if (list.length == 0) {
				$('#alertNoOrders').show();
			} else {
				$('#tblCustomerOrders').DataTable().clear().destroy();
				$('#tblCustomerOrders tbody').html('');
				$.each(list, function(index, order) {
					var row = '<tr>';
					row += '<td>' + order.id + '</td>';
					row += '<td>' + order.description + '</td>';
					row += '<td>&euro;' + order.price.toFixed(2) + '</td>';
					row += '<td>' + order.dateCreated + '</td>';
					var badgeType;
					switch (order.status) {
					case 'PLACED':
						badgeType = 'primary';
						break;
					case 'PAID':
						badgeType = 'success';
						break;
					case 'SERVED':
						badgeType = 'warning';
						break;
					case 'COLLECTED':
						badgeType = 'info';
						break;
					default:
						badgeType = 'info';
					}
					row += '<td><span class="badge badge-' + badgeType + '">' + order.status + '</span></td>';
					row += '<td>';
					if (order.status == 'PLACED') {
						row += '<a href="#" data-toggle="tooltip" title="Pay Bill" class="btnPayOrder" onclick="tblCustomerOrders_btnPay_Click(' + order.id + ',' + order.price + ');"><i data-feather="credit-card"></i></a>';
						row += '&nbsp;';
						row += '<a href="#" data-toggle="tooltip" title="Change Order" class="btnEditOrder" onclick="tblCustomerOrders_btnUpdate_Click(' + order.id + ');"><i data-feather="edit"></i></a>';
						row += '&nbsp;';
						row += '<a href="#" data-toggle="tooltip" title="Cancel Order" class="btnDeleteOrder" onclick="tblCustomerOrders_btnCancel_Click(' + order.id + ');"><i data-feather="x-circle"></i></a>';
					} else {
						row += '<a data-toggle="tooltip" title="Order cannot be updated after payment" class="disabled"><i data-feather="slash"></i></a>';
					}
					row += '</td>';
					row += '</tr>';
					$('#tblCustomerOrders tbody').append(row);
				});
				$('#tblCustomerOrders').DataTable({ order: [[ 3, 'desc' ]], searching: false, lengthChange: false });
				$('#tblCustomerOrders a').tooltip();  // load the tooltips
			    feather.replace();  // load the icons
				$('#tblCustomerOrders').show();
				$('#spnLastRefresh').text('Last refreshed ' + new Date().toLocaleString());
			}
		},
		error : function(response) {
			var message = response.status + " " + response.statusText;
			alert(message);
		},
		complete : function() {

		}
	});
}

function tblCustomerOrders_btnPay_Click(orderId, amount) {
	$('#frmPayment_OrderID').val(orderId);
	$('#frmPayment_Amount').val(amount.toFixed(2));
	$('#paymentModal').modal();
}

function frmPayment_btnPay_Click() {
	var orderId = $('#frmPayment_OrderID').val();
    $.ajax({
        url: baseUri + '/customer/orders/' + orderId + '/payment',
        method: 'POST',
        contentType: 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
        success: function(response) {
        	
        },
        error: function(response) {
			var message = response.status + " " + response.statusText;
			alert(message);
        },
		complete : function() {
			populateOrdersTable();
			$('#paymentModal').modal('hide');
		}
    });
}

function tblCustomerOrders_btnCancel_Click(orderId) {
	if (confirm('Are you sure you want to cancel order ' + orderId + '?', 'Cancel order')) { 
	    $.ajax({
	        url: baseUri + '/customer/orders/' + orderId,
	        method: 'DELETE',
	        contentType: 'application/json',
			beforeSend : function(request) {
				request.setRequestHeader('Authorization', 'Basic ' + auth);
			},
	        success: function(response) {
	        	
	        },
	        error: function(response) {
				var message = response.status + " " + response.statusText;
				alert(message);
	        },
			complete : function() {
				populateOrdersTable();
			}
	    });
	}
}

function btnOrderCoffee_Click() {
	$('#frmOrder_UpdateOrderID').val('');
	$('#frmOrder_btnPlaceOrder').show();
	$('#frmOrder_btnUpdateOrder').hide();
	$('#orderModal').modal();
}

function tblCustomerOrders_btnUpdate_Click(orderId) {
	$('#frmOrder_UpdateOrderID').val(orderId);
	$('#frmOrder_btnUpdateOrder').show();
	$('#frmOrder_btnPlaceOrder').hide();
	$('#orderModal').modal();
}

$(document).ready(function() {
	// Precalculate price.
	frmOrder_Change();
	
	$('#btnOrderCoffee').click(btnOrderCoffee_Click);
	$('#btnRefreshOrders').click(populateOrdersTable);
	
	// Update price whenever the order form is changed.
	$('#frmOrder input, #frmOrder select').change(frmOrder_Change);

	$('#frmOrder_btnPlaceOrder').click(frmOrder_btnPlaceOrder_Click);
	$('#frmOrder_btnUpdateOrder').click(frmOrder_btnUpdateOrder_Click);
	
	$('#frmPayment').submit(function(event){
		event.preventDefault();
		frmPayment_btnPay_Click();
	});
	
	$.ajax({
		url : baseUri + '/customer',
		method : 'GET',
		contentType : 'application/json',
		beforeSend : function(request) {
			request.setRequestHeader('Authorization', 'Basic ' + auth);
		},
		success : function(data) {
			loggedInUser = data;
			$('#welcomeText').html('Welcome, ' + loggedInUser.username + '!');
			populateOrdersTable();
		},
		error : function(response) {
			window.location = "signin.html";
		},
		complete : function() {

		}
	});
});