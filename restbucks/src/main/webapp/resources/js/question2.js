(function() {
	document.addEventListener('DOMContentLoaded', makeRequest);
	var httpRequest;

	function makeRequest() {
		// Load authorization token set from the login page.
		var auth = window.sessionStorage.getItem('auth');
		
		httpRequest = new XMLHttpRequest();
		if (!httpRequest) {
			alert('Error: Cannot create an XMLHttpRequest instance');
			return false;
		}
		
		httpRequest.onreadystatechange = handleResponse;
		httpRequest.open('GET', '/restbucks/rest/barista/orders');
		httpRequest.setRequestHeader('Authorization', 'Basic ' + auth);
		httpRequest.send();
	}

	function handleResponse() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				var orderList = JSON.parse(httpRequest.responseText);
				populateOrdersTable(orderList);
			} else {
				alert('There was a problem with the request.');
			}
		}
	}
	
	function populateOrdersTable(orderList) {
		var html = '';
		orderList.forEach(function(order) {
			html += '<tr>'
				+ '<td>' + order.id + '</td>'
				+ '<td>' + order.customerId + '</td>'
				+ '<td>' + order.description + '</td>'
				+ '<td>&euro;' + order.price.toFixed(2) + '</td>'
				+ '<td>' + order.dateCreated + '</td>'
				+ '<td>' + order.status + '</td>'
				+ '</tr>';
		});
		var tbody = document.querySelector('#tblCustomerOrders > tbody');
		tbody.innerHTML = html;
	}
})();
