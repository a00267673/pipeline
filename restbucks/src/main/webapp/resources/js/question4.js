var app = new Vue({
    el: '#app',

    data: {
        title: 'All Customer Orders (Vue.js)',
        orders: []
    },

    methods: {
        findAllOrders() {
        	var auth = window.sessionStorage.getItem('auth');
            fetch('/restbucks/rest/barista/orders', 
            	{ headers: { 'Authorization': 'Basic ' + auth }
            })
            .then(response => response.json())
            .then(json => this.orders = json);
        }
    },

    mounted() {
        this.findAllOrders()
    }
});