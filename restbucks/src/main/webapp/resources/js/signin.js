// Upon successful login, this saves the 'auth' string in session storage.
// It can then be used on each subsequent REST API call.
function frmLogin_btnSignIn_Click() {
	var username = $('#frmLogin_Username').val();
	var password = $('#frmLogin_Password').val();
	var auth = btoa(username + ':' + password);
	// Disable the submit button.
	$('#frmLogin_btnSignIn').prop('disabled', true);
    $.ajax({
        url: '/restbucks/rest/customer',
        method: 'GET',
        contentType: 'application/json',
    	beforeSend : function(request) {
    		request.setRequestHeader('Authorization', 'Basic ' + auth);
    	},
        success: function(response) {
        	window.sessionStorage.setItem('auth', auth);
            window.location = response.role.toLowerCase() + ".html";
        },
        error: function(response) {
        	var message;
        	if (response.status == 401) {
        		message = "Incorrect username or password. Please try again.";
        	} else {
        		message = response.status + " " + response.statusText;
        	}
        	$('#frmLogin_Error').text(message);
            $('#frmLogin_Error').show();
        },
        complete: function() {
        	// Re-enable the submit button.
        	$('#frmLogin_btnSignIn').prop('disabled', false);
        }
    });
}
