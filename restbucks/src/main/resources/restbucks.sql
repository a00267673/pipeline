DROP DATABASE IF EXISTS RestbucksTestDS;
CREATE DATABASE RestbucksTestDS;

DROP DATABASE IF EXISTS RestbucksDS;
CREATE DATABASE RestbucksDS;
USE RestbucksDS;

CREATE TABLE users (
    user_id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL,
    password VARCHAR(64) NOT NULL,
    role VARCHAR(64) NOT NULL,
    PRIMARY KEY (user_id)
);
CREATE TABLE orders (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    dateCreated TIMESTAMP NOT NULL,
    price DECIMAL(19,4) NOT NULL,
    status VARCHAR(64) NOT NULL,
    drinkSize VARCHAR(64) NOT NULL,
    drinkType VARCHAR(64) NOT NULL,
    withMilk VARCHAR(64) NOT NULL,
    withSugar VARCHAR(64) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES users (user_id)
);

INSERT INTO users (user_id, username, password, role) VALUES (1, 'admin', 'password', 'BARISTA');
INSERT INTO users (user_id, username, password, role) VALUES (2, 'danny', 'password', 'CUSTOMER');
INSERT INTO users (user_id, username, password, role) VALUES (3, 'alice', 'password', 'CUSTOMER');

INSERT INTO orders (id, user_id, dateCreated, price, status, drinkSize, drinkType, withMilk, withSugar) VALUES (1, 2, '2020-03-22 13:31', 2.50, 'PLACED', 'SMALL', 'AMERICANO', 'SOY_MILK', 'WITH_SUGAR');
-- INSERT INTO orders (id, user_id, dateCreated, price, status, drinkSize, drinkType, withMilk, withSugar) VALUES (2, 2, '2020-03-22 14:42', 3.50 'PLACED', 'LARGE', 'LATTE', 'NO_MILK', 'WITH_SUGAR');

-- SELECT * FROM users;
-- SELECT * FROM orders;
